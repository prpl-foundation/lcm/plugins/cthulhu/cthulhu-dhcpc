# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.3.0 - 2024-03-11(14:15:41 +0000)

### Other

- wait longer to get ip address

## Release v1.2.4 - 2023-12-01(09:08:39 +0000)

### Other

- check if string is not empty before accessing

## Release v1.2.3 - 2023-11-06(12:14:59 +0000)

### Fixes

- All amxm destructors should let top application unloading them

## Release v1.2.3 - 2023-11-06(09:35:32 +0000)

### Fixes

- All amxm destructors should let top application unloading them

## Release v1.2.2 - 2023-10-04(13:46:40 +0000)

### Other

- Opensource component

## Release v1.2.1 - 2023-09-29(12:50:59 +0000)

## Release v1.0.1 - 2023-05-26(14:06:13 +0000)

## Release v1.0.0 - 2023-05-26(08:48:57 +0000)

### Other

- create a netns and run dhcpc for each container

## Release v0.0.3 - 2023-05-08(12:27:20 +0000)

### Other

- replace bash with sh

## Release v0.0.2 - 2022-12-23(15:09:19 +0000)

