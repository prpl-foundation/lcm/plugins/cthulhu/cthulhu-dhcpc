include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C test clean

install: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(DEST)$(LIBDIR)/amx/cthulhu/plugins/$(COMPONENT).so
	$(INSTALL) -d -m 0755 $(DEST)/$(LIBDIR)/amx/cthulhu/plugins/mibs
	$(foreach odl,$(wildcard ./mibs/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)$(LIBDIR)/amx/cthulhu/plugins/mibs/;)
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/cthulhu/defaults.d
	$(foreach odl,$(wildcard odl/defaults.d/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/cthulhu/defaults.d/;)
	$(INSTALL) -d -m 0755 $(DEST)/$(LIBDIR)/amx/cthulhu/plugins/dhcpc
	$(INSTALL) -D -p -m 0755 config/*.script $(DEST)$(LIBDIR)/amx/cthulhu/plugins/dhcpc/

package: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)$(LIBDIR)/amx/cthulhu/plugins/$(COMPONENT).so
	$(INSTALL) -d -m 0755 $(PKGDIR)/$(LIBDIR)/amx/cthulhu/plugins/mibs
	$(INSTALL) -D -p -m 0644 ./mibs/*.odl $(PKGDIR)$(LIBDIR)/amx/cthulhu/plugins/mibs/
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/cthulhu/defaults.d
	$(INSTALL) -D -p -m 0644 odl/defaults.d/*.odl $(PKGDIR)/etc/amx/cthulhu/defaults.d/
	$(INSTALL) -d -m 0755 $(PKGDIR)/$(LIBDIR)/amx/cthulhu/plugins/dhcpc
	$(INSTALL) -D -p -m 0755 config/*.script $(PKGDIR)$(LIBDIR)/amx/cthulhu/plugins/dhcpc/
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(MAKE) -C doc doc

	# expand/substitute source wildcard instead of using destination directory: the destination directory can contain files from another artifact not intended for pcb_docgen use
	$(eval ODLFILES += $(wildcard odl/defaults.d/*.odl))

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test