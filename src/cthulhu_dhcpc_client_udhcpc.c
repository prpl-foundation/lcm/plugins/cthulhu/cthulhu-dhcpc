/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE 1 // to make strdup work

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxb/amxb.h>
#include <debug/sahtrace.h>
#include <cthulhu/cthulhu_helpers.h>
#include <cthulhu/cthulhu_defines.h>

#include "cthulhu_dhcpc_assert.h"
#include "cthulhu_dhcpc_client_udhcpc.h"


#define ME "plugin"

#define free_null(p) free(p); p = NULL

static amxc_htable_t * udhcpc_data_table = NULL;
static amxb_bus_ctx_t* amxb_bus_ctx_cthulhu = NULL;

typedef struct _udhcpc_data {
    amxc_htable_it_t hit;
    amxc_string_t udhcpc_dir;
    amxc_llist_t pidfiles;
    amxc_string_t logfile;
    amxc_string_t rootfs_dir;
    amxc_string_t etc_dir;
    bool valid;
} udhcpc_data_t;

static int udhcpc_data_init(udhcpc_data_t* const udhcpc_data) {
    int retval = -1;
    when_null(udhcpc_data, exit);
    amxc_string_init(&udhcpc_data->udhcpc_dir, 0);
    amxc_llist_init(&udhcpc_data->pidfiles);
    amxc_string_init(&udhcpc_data->logfile, 0);
    amxc_string_init(&udhcpc_data->rootfs_dir, 0);
    amxc_string_init(&udhcpc_data->etc_dir, 0);
    udhcpc_data->valid = false;

    retval = 0;
exit:
    return retval;
}

static int udhcpc_data_clean(udhcpc_data_t* const udhcpc_data) {
    int retval = -1;
    when_null(udhcpc_data, exit);
    amxc_string_clean(&udhcpc_data->udhcpc_dir);
    amxc_llist_clean(&udhcpc_data->pidfiles, amxc_string_list_it_free);
    amxc_string_clean(&udhcpc_data->logfile);
    amxc_string_clean(&udhcpc_data->rootfs_dir);
    amxc_string_clean(&udhcpc_data->etc_dir);
    amxc_htable_it_clean(&udhcpc_data->hit, NULL);

    retval = 0;
exit:
    return retval;
}

static int udhcpc_data_new(udhcpc_data_t** udhcpc_data_p) {
    int retval = -1;
    when_null(udhcpc_data_p, exit);

    *udhcpc_data_p = (udhcpc_data_t*) calloc(1, sizeof(udhcpc_data_t));
    when_null(*udhcpc_data_p, exit);

    udhcpc_data_init(*udhcpc_data_p);
    retval = 0;

exit:
    return retval;
}

static void udhcpc_data_delete(udhcpc_data_t** udhcpc_data_p) {
    when_null(udhcpc_data_p, exit);

    udhcpc_data_clean(*udhcpc_data_p);

    free_null(*udhcpc_data_p);
exit:
    return;
}

static udhcpc_data_t* udhcpc_data_get(const char* sb_id) {
    udhcpc_data_t* udhcpc_data = NULL;
    amxc_htable_it_t* it = amxc_htable_get(udhcpc_data_table, sb_id);
    if(it) {
        udhcpc_data = amxc_htable_it_get_data(it, udhcpc_data_t, hit);
    }
    return udhcpc_data;
}

static void udhcpc_data_table_it_free(UNUSED const char* key, amxc_htable_it_t* it) {
    udhcpc_data_t* udhcpc_data = amxc_htable_it_get_data(it, udhcpc_data_t, hit);
    udhcpc_data_delete(&udhcpc_data);
}

static inline amxb_bus_ctx_t* cthulhu_get_amxb_bus_ctx(void) {
    return amxb_bus_ctx_cthulhu;
}

static int update_amxb_bus_ctx_cthulhu(void) {
    int retval = -1;
    amxb_bus_ctx_cthulhu = amxb_be_who_has(CTHULHU_DM);
    if(!amxb_bus_ctx_cthulhu) {
        SAH_TRACEZ_ERROR(ME, "Cannot get bus ctx for " CTHULHU_DM);
        goto exit;
    }

    retval = 0;
exit:
    return retval;
}

static int udhcpc_send_cmd(const char* const ns_name, const char* const command) {
    int rc = 0;
    amxc_var_t exec_args;
    amxc_var_init(&exec_args);

    amxc_var_set_type(&exec_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &exec_args, CTHULHU_SANDBOX_ID, ns_name);
    amxc_var_add_key(cstring_t, &exec_args, CTHULHU_CMD_SB_EXEC_CMD, command);

    rc = amxb_call(cthulhu_get_amxb_bus_ctx(),
                   CTHULHU_DM_SANDBOX,
                   CTHULHU_CMD_SB_EXEC,
                   &exec_args,
                   NULL,
                   0);

    amxc_var_clean(&exec_args);
    return rc;
}

static int udhcpc_init(void) {
    amxc_htable_new(&udhcpc_data_table, 10);
    update_amxb_bus_ctx_cthulhu();
    return 0;
}

static int udhcpc_cleanup(void) {
    amxc_htable_delete(&udhcpc_data_table, udhcpc_data_table_it_free);
    return 0;
}

static int udhcpc_add_ns(const char* ns_name, const char* sb_host_data_dir) {
    int rc = -1;
    when_null(sb_host_data_dir, exit);

    udhcpc_data_t* udhcpc_data = udhcpc_data_get(ns_name);
    if(udhcpc_data) {
        SAH_TRACEZ_INFO(ME, "DHCPC data already exists");
        rc = 0;
        goto exit;
    } else {
        udhcpc_data_new(&udhcpc_data);
        amxc_htable_insert(udhcpc_data_table, ns_name, &udhcpc_data->hit);
    }
    amxc_string_appendf(&udhcpc_data->rootfs_dir, "%s/rootfs", sb_host_data_dir);
    amxc_string_appendf(&udhcpc_data->etc_dir, "%s/etc", udhcpc_data->rootfs_dir.buffer);
    if(!cthulhu_isdir(udhcpc_data->etc_dir.buffer)) {
        if(cthulhu_mkdir(udhcpc_data->etc_dir.buffer, true) < 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to make dir [%s]", udhcpc_data->etc_dir.buffer);
            goto error;
        }
    }

    amxc_string_appendf(&udhcpc_data->udhcpc_dir, "%s/udhcpc", sb_host_data_dir);
    if(cthulhu_mkdir(udhcpc_data->udhcpc_dir.buffer, true) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not make settings dir [%s]", udhcpc_data->udhcpc_dir.buffer);
        amxc_string_reset(&udhcpc_data->udhcpc_dir);
        goto error;
    }
    amxc_string_appendf(&udhcpc_data->logfile, "%s/udhcpc.log", udhcpc_data->udhcpc_dir.buffer);

    rc = 0;
    goto exit;
error:
    amxc_string_reset(&udhcpc_data->udhcpc_dir);
    amxc_string_reset(&udhcpc_data->rootfs_dir);
    amxc_string_reset(&udhcpc_data->etc_dir);
    amxc_string_reset(&udhcpc_data->logfile);
exit:
    return rc;
}

static int udhcpc_add_itf(const char* ns_name, const char* itf_name, const char* preferred_ip) {
    int rc = -1;
    amxc_string_t command;
    amxc_string_init(&command, 0);

    udhcpc_data_t* udhcpc_data = udhcpc_data_get(ns_name);
    if(!udhcpc_data) {
        SAH_TRACEZ_ERROR(ME, "No udhcpc data exists for namespace [%s]", ns_name);
        goto exit;
    }

    amxc_string_t* pidfile;
    amxc_string_new(&pidfile, 0);
    amxc_string_appendf(pidfile, "%s/%s.pid", udhcpc_data->udhcpc_dir.buffer, itf_name);
    unlink(pidfile->buffer);
    if(amxc_llist_append(&udhcpc_data->pidfiles, &pidfile->it) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to add pidfile [%s] to list", pidfile->buffer);
        amxc_string_delete(&pidfile);
        goto exit;
    }

    if(cthulhu_isdir("/sys/kernel/security/apparmor")) {
        amxc_string_appendf(&command, "aa-exec -p unconfined ");
    }
    amxc_string_appendf(&command, "udhcpc -n -t 40 -s /usr/lib/amx/cthulhu/plugins/dhcpc/udhcpc.script -p %s -i %s", pidfile->buffer, itf_name);
    if(!string_is_empty(preferred_ip)) {
        amxc_string_appendf(&command, " -r %s", preferred_ip);
    }
    amxc_string_appendf(&command, " >> %s 2>&1", udhcpc_data->logfile.buffer);

    rc = udhcpc_send_cmd(ns_name, command.buffer);

exit:

    amxc_string_clean(&command);
    return rc;
}

static char* udhcpc_get_resolvfile(const char* ns_name) {
    char* resolv_file = NULL;

    udhcpc_data_t* udhcpc_data = udhcpc_data_get(ns_name);
    if(!udhcpc_data) {
        SAH_TRACEZ_ERROR(ME, "No udhcpc data exists for namespace [%s]", ns_name);
        return NULL;
    }
    if(asprintf(&resolv_file, "%s/resolv.conf", udhcpc_data->etc_dir.buffer) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create string");
    }
    return resolv_file;
}

static int udhcpc_stop(const char* ns_name) {
    int rc = -1;

    udhcpc_data_t* udhcpc_data = udhcpc_data_get(ns_name);
    if(!udhcpc_data) {
        SAH_TRACEZ_ERROR(ME, "No udhcpc data exists for namespace [%s]", ns_name);
        goto exit;
    }

    amxc_llist_it_t* it = amxc_llist_take_first(&udhcpc_data->pidfiles);
    while(it) {
        amxc_string_t* pidfile = amxc_container_of(it, amxc_string_t, it);
        if(cthulhu_isfile(pidfile->buffer)) {

            amxc_string_t command;
            amxc_string_init(&command, 0);
            amxc_string_appendf(&command, "kill -s USR2 $(cat %s)", pidfile->buffer);
            udhcpc_send_cmd(ns_name, command.buffer);

            amxc_string_reset(&command);
            amxc_string_appendf(&command, "kill -s TERM $(cat %s)", pidfile->buffer);
            udhcpc_send_cmd(ns_name, command.buffer);

            amxc_string_clean(&command);
        } else {
            SAH_TRACEZ_ERROR(ME, "Pidfile does not exist [%s]", pidfile->buffer);
        }
        amxc_string_delete(&pidfile);
        it = amxc_llist_take_first(&udhcpc_data->pidfiles);
    }
    rc = 0;
exit:

    return rc;
}

static int udhcpc_remove_ns(const char* ns_name) {
    udhcpc_data_t* udhcpc_data = NULL;
    udhcpc_data = udhcpc_data_get(ns_name);
    if(udhcpc_data) {
        udhcpc_data_delete(&udhcpc_data);
    }
    return 0;
}

static dhcpc_client_funcs_t udhcpc_functions = {
    .init = udhcpc_init,
    .add_ns = udhcpc_add_ns,
    .prestart = NULL,
    .add_itf = udhcpc_add_itf,
    .start = NULL,
    .get_resolvfile = udhcpc_get_resolvfile,
    .stop = udhcpc_stop,
    .remove_ns = udhcpc_remove_ns,
    .cleanup = udhcpc_cleanup
};

dhcpc_client_funcs_t* udhcpc_get_functions(void) {
    return &udhcpc_functions;
}
