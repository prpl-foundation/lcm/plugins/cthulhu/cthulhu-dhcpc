/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE 1 // to make strdup work

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxm/amxm.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>
#include <amxo/amxo.h>
#include <amxc/amxc_macros.h>

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>
#include <cthulhu/cthulhu_defines_plugin.h>
#include <cthulhu/cthulhu_helpers.h>
#include <lcm/lcm_assert.h>
#include <debug/sahtrace.h>
#include <libocispec/image_spec_schema_image_manifest_schema.h>

#include "cthulhu_dhcpc.h"
// #include "cthulhu_dhcpc_assert.h"
#include "cthulhu_dhcpc_defines.h"
#include "cthulhu_dhcpc_client_dhclient.h"
#include "cthulhu_dhcpc_client_udhcpc.h"

#define ME "plugin"

static amxm_shared_object_t* so = NULL;
static amxm_module_t* mod = NULL;
static amxd_dm_t* dm = NULL;
static amxb_bus_ctx_t* amxb_bus_ctx_cthulhu = NULL;

static dhcpc_client_funcs_t* dhcpc_client_funcs = NULL;


static amxd_object_t* dhcpc_sb_get(const char* sb_id) {
    return amxd_dm_findf(dm, CTHULHU_DM_SANDBOX_INSTANCES ".[" CTHULHU_SANDBOX_ID " == '%s'].", sb_id);
}
static amxd_object_t* dhcpc_ctr_get(const char* ctr_id) {
    return amxd_dm_findf(dm, CTHULHU_DM_CONTAINER_INSTANCES ".[" CTHULHU_CONTAINER_ID " == '%s'].", ctr_id);
}

static inline amxd_object_t* dhcpc_get_itf_settings(amxd_object_t* sb_obj, const char* itf_name) {
    return amxd_object_findf(sb_obj, CTHULHU_PLUGINS "." DHCP "." DHCP_INTERFACES ".[ " DHCP_INTERFACES_INTERFACE " == '%s'].", itf_name);
}

static inline amxd_object_t* dhcpc_get_settings(amxd_object_t* sb_obj) {
    return amxd_object_findf(sb_obj, CTHULHU_PLUGINS "." DHCP ".");
}

static int dhcp_create_itf_settings(amxd_object_t* sb_obj, const char* itf_name) {
    int rc = -1;
    amxd_trans_t trans;
    amxd_object_t* plugin_itfs = NULL;
    amxd_object_t* plugin = NULL;
    amxd_object_t* instance = NULL;
    bool default_enabled = false;
    amxd_status_t status = amxd_status_unknown_error;

    amxd_trans_init(&trans);

    plugin = dhcpc_get_settings(sb_obj);
    when_null_log(plugin, exit);
    instance = dhcpc_get_itf_settings(sb_obj, itf_name);
    if(instance) {
        // instance already exists, do nothing
        rc = 0;
        goto exit;
    }

    default_enabled = amxd_object_get_bool(plugin, DHCP_DEFAULTENABLED, NULL);
    plugin_itfs = amxd_object_get_child(plugin, DHCP_INTERFACES);
    if(!plugin_itfs) {
        SAH_TRACEZ_ERROR(ME, "Could not find DHCP.Interfaces");
        goto exit;
    }

    amxd_trans_select_object(&trans, plugin_itfs);
    amxd_trans_add_inst(&trans, 0, itf_name);
    amxd_trans_set_cstring_t(&trans, DHCP_INTERFACES_INTERFACE, itf_name);
    amxd_trans_set_bool(&trans, DHCP_INTERFACES_ENABLEDHCP, default_enabled);

    status = amxd_trans_apply(&trans, dm);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed. Status: %d", status);
        goto exit;
    }

    rc = 0;
exit:
    amxd_trans_clean(&trans);
    return rc;
}


/**
 * @brief DHCP is enabled if the networkNS is enabled and type == Veth
 *
 * @param sb_obj
 * @return true
 * @return false
 */
static bool dhcpc_enable_ns(amxd_object_t* sb_obj) {
    bool res = false;

    amxd_object_t* netns_obj = amxd_object_findf(sb_obj, CTHULHU_SANDBOX_NETWORK_NS);
    if(!netns_obj) {
        SAH_TRACEZ_ERROR(ME, "Could not find "CTHULHU_SANDBOX_NETWORK_NS);
        goto exit;
    }
    bool enabled = amxd_object_get_bool(netns_obj, CTHULHU_SANDBOX_NETWORK_NS_ENABLE, NULL);
    if(!enabled) {
        goto exit;
    }
    char* type = amxd_object_get_cstring_t(netns_obj, CTHULHU_SANDBOX_NETWORK_NS_TYPE, NULL);
    if(strcmp(type, CTHULHU_SANDBOX_NETWORK_NS_TYPE_VETH) == 0) {
        res = true;
    }
    free(type);
exit:
    return res;
}

static void dhcpc_itf_added(UNUSED const char* const sig_name,
                            const amxc_var_t* const data,
                            UNUSED void* const priv) {
    const char* path = GET_CHAR(data, "path");
    if(string_is_empty(path)) {
        return;
    }

    amxd_object_t* obj = amxd_dm_findf(dm, "%s", path);
    if(!obj) {
        SAH_TRACEZ_ERROR(ME, "Path not found");
        return;
    }
    amxc_var_t* parameters = amxc_var_get_path(data, "parameters", AMXC_VAR_FLAG_DEFAULT);
    const char* itf_name = GET_CHAR(parameters, CTHULHU_SANDBOX_NETWORK_NS_INTERFACES_INTERFACE);

    if(string_is_empty(itf_name)) {
        SAH_TRACEZ_ERROR(ME, "Interface name not found");
        return;
    }

    amxd_object_t* sb_obj = amxd_object_findf(obj, "^.^.");
    amxd_object_t* dhcp_settings = dhcpc_get_itf_settings(sb_obj, itf_name);
    if(!dhcp_settings) {
        SAH_TRACEZ_INFO(ME, "Create new DHCP settings for %s", itf_name);
        dhcp_create_itf_settings(sb_obj, itf_name);
    }
}

static void dhcpc_itf_removed(UNUSED const char* const sig_name,
                              const amxc_var_t* const data,
                              UNUSED void* const priv) {
    const char* path = GET_CHAR(data, "path");
    if(string_is_empty(path)) {
        return;
    }

    amxd_object_t* obj = amxd_dm_findf(dm, "%s", path);
    if(!obj) {
        SAH_TRACEZ_ERROR(ME, "Path not found");
        return;
    }
    amxc_var_t* parameters = amxc_var_get_path(data, "parameters", AMXC_VAR_FLAG_DEFAULT);
    const char* itf_name = GET_CHAR(parameters, CTHULHU_SANDBOX_NETWORK_NS_INTERFACES_INTERFACE);

    if(string_is_empty(itf_name)) {
        SAH_TRACEZ_ERROR(ME, "Interface name not found");
        return;
    }

    amxd_object_t* sb_obj = amxd_object_findf(obj, "^.^.");
    amxd_object_t* dhcp_settings = dhcpc_get_itf_settings(sb_obj, itf_name);
    if(dhcp_settings) {
        amxd_object_delete(&dhcp_settings);
    }
}

static void dhcpc_itf_changed(UNUSED const char* const sig_name,
                              const amxc_var_t* const data,
                              UNUSED void* const priv) {
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_unknown_error;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    const char* path = GET_CHAR(data, "path");
    if(string_is_empty(path)) {
        return;
    }

    amxd_object_t* obj = amxd_dm_findf(dm, "%s", path);
    if(!obj) {
        SAH_TRACEZ_ERROR(ME, "Path not found");
        goto exit;
    }
    amxd_object_t* sb_obj = amxd_object_findf(obj, "^.^.^.");


    amxc_var_t* parameters_itf = amxc_var_get_path(data, "parameters.Interface", AMXC_VAR_FLAG_DEFAULT);
    if(!parameters_itf) {
        goto exit;
    }

    const char* from = GET_CHAR(parameters_itf, "from");
    const char* to = GET_CHAR(parameters_itf, "to");
    if(string_is_empty(from) || string_is_empty("to")) {
        goto exit;
    }
    amxd_object_t* dhcp_settings = dhcpc_get_itf_settings(sb_obj, from);
    if(!dhcp_settings) {
        SAH_TRACEZ_ERROR(ME, "DHCP settings not found for %s", from);
        goto exit;
    }

    amxd_trans_select_object(&trans, dhcp_settings);
    amxd_trans_set_cstring_t(&trans, "Interface", to);

    status = amxd_trans_apply(&trans, dm);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed. Status: %d", status);
        goto exit;
    }

exit:
    amxd_trans_clean(&trans);
}

static void dhcpc_config_client(void) {
    // check if udhcpc exists
    if(system("which udhcpc > /dev/null 2>&1") == 0) {
        SAH_TRACEZ_INFO(ME, "udhcpc found");
        dhcpc_client_funcs = udhcpc_get_functions();
    } else if(system("which dhclient > /dev/null 2>&1") == 0) {
        SAH_TRACEZ_INFO(ME, "dhclient found");
        dhcpc_client_funcs = dhclient_get_functions();
    } else {
        SAH_TRACEZ_WARNING(ME, "No known client found");
    }
}

static char* dhcpc_get_resolv_file(const char* sb_id) {
    amxd_object_t* sb_obj = NULL;
    char* resolv_file = NULL;

    sb_obj = dhcpc_sb_get(sb_id);
    when_null(sb_obj, exit);
    if(dhcpc_enable_ns(sb_obj)) {
        resolv_file = dhcpc_client_funcs->get_resolvfile ? dhcpc_client_funcs->get_resolvfile(sb_id) : NULL;
        goto exit;
    }
    char* parent_sb = amxd_object_get_cstring_t(sb_obj, CTHULHU_SANDBOX_PARENT, NULL);
    if(!string_is_empty(parent_sb)) {
        resolv_file = dhcpc_get_resolv_file(parent_sb);
    }
    free(parent_sb);

exit:
    return resolv_file;
}

static int dhcpc_init(UNUSED const char* function_name, UNUSED amxc_var_t* args, amxc_var_t* ret) {
    int rc = -1;
    amxc_var_t* var_dm = NULL;
    var_dm = GET_ARG(args, CTHULHU_PLUGIN_ARG_DATAMODEL);
    if(var_dm && var_dm->data.data) {
        dm = (amxd_dm_t*) var_dm->data.data;
    }
    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(ret, CTHULHU_PLUGIN_ARG_MIB_SANDBOX, "dhcpc_sandbox");

    amxb_bus_ctx_cthulhu = amxb_be_who_has(CTHULHU_DM);
    if(!amxb_bus_ctx_cthulhu) {
        SAH_TRACEZ_ERROR(ME, "Cannot get bus ctx for " CTHULHU_DM);
        goto exit;
    }

    dhcpc_config_client();
    rc = 0;
    if(dhcpc_client_funcs) {
        rc |= dhcpc_client_funcs->init ? dhcpc_client_funcs->init() : 0;
    }

    rc |= amxb_subscribe(amxb_bus_ctx_cthulhu,
                         CTHULHU_DM_SANDBOX_INSTANCES ".*." CTHULHU_SANDBOX_NETWORK_NS "." CTHULHU_SANDBOX_NETWORK_NS_INTERFACES ".",
                         "notification == 'dm:instance-added'",
                         dhcpc_itf_added,
                         NULL);

    rc |= amxb_subscribe(amxb_bus_ctx_cthulhu,
                         CTHULHU_DM_SANDBOX_INSTANCES ".*." CTHULHU_SANDBOX_NETWORK_NS "." CTHULHU_SANDBOX_NETWORK_NS_INTERFACES ".",
                         "notification == 'dm:instance-removed'",
                         dhcpc_itf_removed,
                         NULL);
    rc |= amxb_subscribe(amxb_bus_ctx_cthulhu,
                         CTHULHU_DM_SANDBOX_INSTANCES ".*." CTHULHU_SANDBOX_NETWORK_NS "." CTHULHU_SANDBOX_NETWORK_NS_INTERFACES ".",
                         "notification == 'dm:object-changed'",
                         dhcpc_itf_changed,
                         NULL);

exit:
    return rc;
}

static int dhcpc_cleanup(UNUSED const char* function_name, UNUSED amxc_var_t* args, amxc_var_t* ret) {
    int rc = 0;
    if(dhcpc_client_funcs) {
        rc = dhcpc_client_funcs->cleanup ? dhcpc_client_funcs->cleanup() : 0;
    }
    amxc_var_set(int32_t, ret, rc);
    return rc;
}

static int dhcpc_sb_prestart(UNUSED const char* function_name, UNUSED amxc_var_t* args, amxc_var_t* ret) {
    int rc = -1;
    amxd_object_t* sb_obj = NULL;

    if(!dhcpc_client_funcs) {
        rc = 0;
        goto exit;
    }

    const char* sb_id = GET_CHAR(args, CTHULHU_PLUGIN_ARG_SB_ID);
    const char* sb_host_data_dir = GET_CHAR(args, CTHULHU_PLUGIN_ARG_SB_HOST_DATA_DIR);
    if(!sb_id) {
        SAH_TRACEZ_ERROR(ME, CTHULHU_PLUGIN_ARG_SB_ID " is not defined");
        goto exit;
    }
    sb_obj = dhcpc_sb_get(sb_id);
    when_null(sb_obj, exit);

    if(!dhcpc_enable_ns(sb_obj)) {
        rc = 0;
        goto exit;
    }

    rc = dhcpc_client_funcs->add_ns ? dhcpc_client_funcs->add_ns(sb_id, sb_host_data_dir) : 0;

exit:
    amxc_var_set(int32_t, ret, rc);
    return rc;
}

static int dhcpc_sb_poststart(UNUSED const char* function_name, UNUSED amxc_var_t* args, amxc_var_t* ret) {
    int rc = -1;
    amxd_object_t* net_ns = NULL;
    amxd_object_t* dhcp_template = NULL;
    amxd_object_t* sb_obj = NULL;
    if(!dhcpc_client_funcs) {
        rc = 0;
        goto exit;
    }
    const char* sb_id = GET_CHAR(args, CTHULHU_PLUGIN_ARG_SB_ID);
    if(!sb_id) {
        SAH_TRACEZ_ERROR(ME, CTHULHU_PLUGIN_ARG_SB_ID " is not defined");
        goto exit;
    }

    sb_obj = dhcpc_sb_get(sb_id);
    if(!sb_obj) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not find sandbox in datamodel", sb_id);
        goto exit;
    }
    if(!dhcpc_enable_ns(sb_obj)) {
        rc = 0;
        goto exit;
    }

    // check for new interfaces, this can happen after a clean boot
    net_ns = amxd_object_get_child(sb_obj, CTHULHU_SANDBOX_NETWORK_NS);
    if(net_ns) {
        amxd_object_t* interfaces = amxd_object_get_child(net_ns, CTHULHU_SANDBOX_NETWORK_NS_INTERFACES);
        amxd_object_for_each(instance, it, interfaces) {
            amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
            char* itf = amxd_object_get_cstring_t(instance, CTHULHU_SANDBOX_NETWORK_NS_INTERFACES_INTERFACE, NULL);
            dhcp_create_itf_settings(sb_obj, itf);
            free(itf);
        }
    }

    rc = dhcpc_client_funcs->prestart ? dhcpc_client_funcs->prestart(sb_id) : 0;

    dhcp_template = amxd_object_findf(sb_obj, CTHULHU_PLUGINS "." DHCP "." DHCP_INTERFACES);
    if(dhcp_template == NULL) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not get " CTHULHU_PLUGINS "." DHCP "." DHCP_INTERFACES, sb_id);
        goto exit;
    }
    amxd_object_for_each(instance, it, dhcp_template) {
        amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        bool enable = amxd_object_get_bool(instance, DHCP_INTERFACES_ENABLEDHCP, NULL);
        if(enable) {
            char* itf_name = amxd_object_get_cstring_t(instance, DHCP_INTERFACES_INTERFACE, NULL);
            char* preferred_ip = amxd_object_get_cstring_t(instance, DHCP_INTERFACES_PREFERREDIP, NULL);
            rc = dhcpc_client_funcs->add_itf? dhcpc_client_funcs->add_itf(sb_id, itf_name, preferred_ip) : 0;
            if(rc < 0) {
                SAH_TRACEZ_WARNING(ME, "add_itf returned [%d]", rc);
            }
            free(itf_name);
            free(preferred_ip);
        }
    }

    rc = dhcpc_client_funcs->start ? dhcpc_client_funcs->start(sb_id) : 0;

exit:
    amxc_var_set(int32_t, ret, rc);
    return rc;
}

static int dhcpc_sb_prestop(UNUSED const char* function_name, UNUSED amxc_var_t* args, amxc_var_t* ret) {
    int rc = -1;
    amxd_object_t* sb_obj = NULL;
    if(!dhcpc_client_funcs) {
        rc = 0;
        goto exit;
    }

    const char* sb_id = GET_CHAR(args, CTHULHU_PLUGIN_ARG_SB_ID);
    if(!sb_id) {
        SAH_TRACEZ_ERROR(ME, CTHULHU_PLUGIN_ARG_SB_ID " is not defined");
        goto exit;
    }
    sb_obj = dhcpc_sb_get(sb_id);
    if(!sb_obj) {
        SAH_TRACEZ_ERROR(ME, "SB[%s]: Could not find sandbox in datamodel", sb_id);
        goto exit;
    }
    if(!dhcpc_enable_ns(sb_obj)) {
        rc = 0;
        goto exit;
    }

    rc = dhcpc_client_funcs->stop ? dhcpc_client_funcs->stop(sb_id) : 0;

exit:
    amxc_var_set(int32_t, ret, rc);
    return rc;
}

static int dhcpc_sb_postremove(UNUSED const char* function_name, UNUSED amxc_var_t* args, amxc_var_t* ret) {
    int rc = -1;
    if(!dhcpc_client_funcs) {
        rc = 0;
        goto exit;
    }
    const char* sb_id = GET_CHAR(args, CTHULHU_PLUGIN_ARG_SB_ID);
    if(!sb_id) {
        SAH_TRACEZ_ERROR(ME, CTHULHU_PLUGIN_ARG_SB_ID " is not defined");
        goto exit;
    }

    rc = dhcpc_client_funcs->remove_ns? dhcpc_client_funcs->remove_ns(sb_id) : 0;

exit:
    amxc_var_set(int32_t, ret, rc);
    return rc;
}

static int dhcpc_ctr_prestart(UNUSED const char* function_name, amxc_var_t* args, amxc_var_t* ret) {
    int rc = -1;
    cthulhu_ctr_start_data_t* start_data = NULL;
    amxd_object_t* ctr_obj = NULL;
    char* priv_sb_id = NULL;

    if(!dhcpc_client_funcs) {
        rc = 0;
        goto exit;
    }

    const char* ctr_id = GET_CHAR(args, CTHULHU_PLUGIN_ARG_CTR_ID);
    amxc_var_t* var_start_data = GET_ARG(args, CTHULHU_PLUGIN_ARG_START_DATA);
    if(var_start_data && var_start_data->data.data) {
        start_data = (cthulhu_ctr_start_data_t*) var_start_data->data.data;
    } else {
        goto exit;
    }

    ctr_obj = dhcpc_ctr_get(ctr_id);
    if(!ctr_obj) {
        SAH_TRACEZ_ERROR(ME, "CTR[%s]: Container not found in datamodel", ctr_id);
        goto exit;
    }

    priv_sb_id = amxd_object_get_cstring_t(ctr_obj, CTHULHU_CONTAINER_PRIVATESANDBOX, NULL);
    if(string_is_empty(priv_sb_id)) {
        goto exit;
    }

    char* resolv_file = dhcpc_get_resolv_file(priv_sb_id);
    if(resolv_file && cthulhu_isfile(resolv_file)) {
        SAH_TRACEZ_INFO(ME, "CTR[%s]: set resolv file %s", ctr_id, resolv_file);
        cthulhu_ctr_start_data_add_mount_data(start_data, resolv_file, "/etc/resolv.conf", NULL, NULL);
    }
    free(resolv_file);
    rc = 0;
exit:
    free(priv_sb_id);
    amxc_var_set(int32_t, ret, rc);
    return rc;
}

static int register_function(const char* const func_name, amxm_callback_t cb) {
    int res = -1;
    if(amxm_module_add_function(mod, func_name, cb) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not add function [%s]", func_name);
        goto exit;
    }
    res = 0;
exit:
    return res;
}

AMXM_CONSTRUCTOR module_init(void) {
    SAH_TRACEZ_INFO(ME, ">>> Cthulhu DHCPC constructor\n");
    int ret = 1;

    so = amxm_so_get_current();
    if(so == NULL) {
        SAH_TRACEZ_INFO(ME, "Could not get shared object\n");
        goto exit;
    }

    if(amxm_module_register(&mod, so, CTHULHU_PLUGIN)) {
        SAH_TRACEZ_INFO(ME, "Could not register module %s\n", CTHULHU_PLUGIN);
        goto exit;
    }
    when_failed(register_function(CTHULHU_PLUGIN_INIT, dhcpc_init), exit);
    when_failed(register_function(CTHULHU_PLUGIN_CLEANUP, dhcpc_cleanup), exit);
    when_failed(register_function(CTHULHU_PLUGIN_CTR_PRESTART, dhcpc_ctr_prestart), exit);

    when_failed(register_function(CTHULHU_PLUGIN_SB_PRESTART, dhcpc_sb_prestart), exit);
    when_failed(register_function(CTHULHU_PLUGIN_SB_POSTSTART, dhcpc_sb_poststart), exit);
    when_failed(register_function(CTHULHU_PLUGIN_SB_PRESTOP, dhcpc_sb_prestop), exit);
    when_failed(register_function(CTHULHU_PLUGIN_SB_POSTREMOVE, dhcpc_sb_postremove), exit);

    ret = 0;
exit:
    SAH_TRACEZ_INFO(ME, "<<< Cthulhu DHCPC constructor\n");
    return ret;
}

AMXM_DESTRUCTOR module_exit(void) {
    SAH_TRACEZ_INFO(ME, ">>> Cthulhu DHCPC destructor\n");
    SAH_TRACEZ_INFO(ME, "<<< Cthulhu DHCPC destructor\n");
    return 0;
}
