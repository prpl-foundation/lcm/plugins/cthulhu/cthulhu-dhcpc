/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE 1 // to make strdup work

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxb/amxb.h>
#include <debug/sahtrace.h>
#include <cthulhu/cthulhu_helpers.h>
#include <cthulhu/cthulhu_defines.h>

#include "cthulhu_dhcpc_assert.h"
#include "cthulhu_dhcpc_client_dhclient.h"


#define ME "plugin"

#define free_null(p) free(p); p = NULL

static amxc_htable_t * dhclient_data_table = NULL;
static amxb_bus_ctx_t* amxb_bus_ctx_cthulhu = NULL;

typedef struct _dhclient_data {
    amxc_htable_it_t hit;
    amxc_string_t dhclient_dir;
    amxc_string_t pidfile;
    amxc_string_t leasesfile;
    amxc_string_t logfile;
    amxc_string_t rootfs_dir;
    amxc_string_t etc_dir;
    amxc_string_t command;
    bool valid;
} dhclient_data_t;


static int dhclient_data_init(dhclient_data_t* const dhclient_data) {
    int retval = -1;
    when_null(dhclient_data, exit);
    amxc_string_init(&dhclient_data->dhclient_dir, 0);
    amxc_string_init(&dhclient_data->pidfile, 0);
    amxc_string_init(&dhclient_data->leasesfile, 0);
    amxc_string_init(&dhclient_data->logfile, 0);
    amxc_string_init(&dhclient_data->rootfs_dir, 0);
    amxc_string_init(&dhclient_data->etc_dir, 0);
    amxc_string_init(&dhclient_data->command, 0);
    dhclient_data->valid = false;

    retval = 0;
exit:
    return retval;
}

static int dhclient_data_clean(dhclient_data_t* const dhclient_data) {
    int retval = -1;
    when_null(dhclient_data, exit);
    amxc_string_clean(&dhclient_data->dhclient_dir);
    amxc_string_clean(&dhclient_data->pidfile);
    amxc_string_clean(&dhclient_data->leasesfile);
    amxc_string_clean(&dhclient_data->logfile);
    amxc_string_clean(&dhclient_data->rootfs_dir);
    amxc_string_clean(&dhclient_data->etc_dir);
    amxc_string_clean(&dhclient_data->command);
    amxc_htable_it_clean(&dhclient_data->hit, NULL);

    retval = 0;
exit:
    return retval;
}

static int dhclient_data_new(dhclient_data_t** dhclient_data_p) {
    int retval = -1;
    when_null(dhclient_data_p, exit);

    *dhclient_data_p = (dhclient_data_t*) calloc(1, sizeof(dhclient_data_t));
    when_null(*dhclient_data_p, exit);

    dhclient_data_init(*dhclient_data_p);
    retval = 0;

exit:
    return retval;
}

static void dhclient_data_delete(dhclient_data_t** dhclient_data_p) {
    when_null(dhclient_data_p, exit);

    dhclient_data_clean(*dhclient_data_p);

    free_null(*dhclient_data_p);
exit:
    return;
}

static dhclient_data_t* dhclient_data_get(const char* sb_id) {
    dhclient_data_t* dhclient_data = NULL;
    amxc_htable_it_t* it = amxc_htable_get(dhclient_data_table, sb_id);
    if(it) {
        dhclient_data = amxc_htable_it_get_data(it, dhclient_data_t, hit);
    }
    return dhclient_data;
}

static void dhclient_data_table_it_free(UNUSED const char* key, amxc_htable_it_t* it) {
    dhclient_data_t* dhclient_data = amxc_htable_it_get_data(it, dhclient_data_t, hit);
    dhclient_data_delete(&dhclient_data);
}

static inline amxb_bus_ctx_t* cthulhu_get_amxb_bus_ctx(void) {
    return amxb_bus_ctx_cthulhu;
}

static int update_amxb_bus_ctx_cthulhu(void) {
    int retval = -1;
    amxb_bus_ctx_cthulhu = amxb_be_who_has(CTHULHU_DM);
    if(!amxb_bus_ctx_cthulhu) {
        SAH_TRACEZ_ERROR(ME, "Cannot get bus ctx for " CTHULHU_DM);
        goto exit;
    }

    retval = 0;
exit:
    return retval;
}


static int dhclient_init(void) {
    amxc_htable_new(&dhclient_data_table, 10);
    update_amxb_bus_ctx_cthulhu();
    return 0;
}

static int dhclient_cleanup(void) {
    amxc_htable_delete(&dhclient_data_table, dhclient_data_table_it_free);
    return 0;
}

static int dhclient_add_ns(const char* ns_name, const char* sb_host_data_dir) {
    int rc = -1;
    when_null(sb_host_data_dir, exit);

    dhclient_data_t* dhclient_data = dhclient_data_get(ns_name);
    if(dhclient_data) {
        SAH_TRACEZ_INFO(ME, "DHCPC data already exists");
        rc = 0;
        goto exit;
    } else {
        dhclient_data_new(&dhclient_data);
        amxc_htable_insert(dhclient_data_table, ns_name, &dhclient_data->hit);
    }
    amxc_string_appendf(&dhclient_data->rootfs_dir, "%s/rootfs", sb_host_data_dir);
    amxc_string_appendf(&dhclient_data->etc_dir, "%s/etc", dhclient_data->rootfs_dir.buffer);
    if(!cthulhu_isdir(dhclient_data->etc_dir.buffer)) {
        if(cthulhu_mkdir(dhclient_data->etc_dir.buffer, true) < 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to make dir [%s]", dhclient_data->etc_dir.buffer);
            goto error;
        }
    }

    amxc_string_appendf(&dhclient_data->dhclient_dir, "%s/dhclient", sb_host_data_dir);
    if(cthulhu_mkdir(dhclient_data->dhclient_dir.buffer, true) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not make settings dir [%s]", dhclient_data->dhclient_dir.buffer);
        amxc_string_reset(&dhclient_data->dhclient_dir);
        goto error;
    }
    amxc_string_appendf(&dhclient_data->logfile, "%s/dhclient.log", dhclient_data->dhclient_dir.buffer);
    amxc_string_appendf(&dhclient_data->pidfile, "%s/dhclient.pid", dhclient_data->dhclient_dir.buffer);
    amxc_string_appendf(&dhclient_data->leasesfile, "%s/dhclient.leases", dhclient_data->dhclient_dir.buffer);
    unlink(dhclient_data->pidfile.buffer);
    unlink(dhclient_data->leasesfile.buffer);

    rc = 0;
    goto exit;
error:
    amxc_string_reset(&dhclient_data->dhclient_dir);
    amxc_string_reset(&dhclient_data->rootfs_dir);
    amxc_string_reset(&dhclient_data->etc_dir);
    amxc_string_reset(&dhclient_data->logfile);
    amxc_string_reset(&dhclient_data->pidfile);
    amxc_string_reset(&dhclient_data->leasesfile);
exit:
    return rc;
}

static int dhclient_prestart(const char* ns_name) {
    int rc = -1;

    dhclient_data_t* dhclient_data = dhclient_data_get(ns_name);
    if(!dhclient_data) {
        SAH_TRACEZ_ERROR(ME, "No dhclient data exists for namespace [%s]", ns_name);
        goto exit;
    }
    amxc_string_reset(&dhclient_data->command);

    // /sbin/dhclient -1 -pf /tmp/settings/dhclient.pid -lf /tmp/settings/dhclient.leases -e ROOTFS=/tmp/rootfs -sf /usr/share/lxc/hooks/dhclient-script -v eth7 eth9 >> "${debugfile}" 2>&1
    if(cthulhu_isdir("/sys/kernel/security/apparmor")) {
        amxc_string_appendf(&dhclient_data->command, "aa-exec -p unconfined dhclient");
    } else {
        amxc_string_appendf(&dhclient_data->command, "dhclient");
    }

    amxc_string_appendf(&dhclient_data->command, " -1 -v -pf %s -lf %s -e ROOTFS=%s -sf /usr/lib/amx/cthulhu/plugins/dhcpc/dhclient.script",
                        dhclient_data->pidfile.buffer, dhclient_data->leasesfile.buffer, dhclient_data->rootfs_dir.buffer);

    unlink(dhclient_data->leasesfile.buffer);
    rc = 0;
exit:
    return rc;

}

static int dhclient_add_itf(const char* ns_name, const char* itf_name, const char* preferred_ip) {
    int rc = -1;
    amxc_string_t lease;
    amxc_string_init(&lease, 0);

    dhclient_data_t* dhclient_data = dhclient_data_get(ns_name);
    if(!dhclient_data) {
        SAH_TRACEZ_ERROR(ME, "No dhclient data exists for namespace [%s]", ns_name);
        goto exit;
    }
    dhclient_data->valid = true;
    amxc_string_appendf(&dhclient_data->command, " %s", itf_name);
    if(!string_is_empty(preferred_ip)) {
        int fd = 0;

        amxc_string_appendf(&lease, "lease {\n  interface \"%s\";\n  fixed-address %s;\n  renew 0 2020/01/01 00:00:01;\n  rebind 0 2020/01/01 00:00:01;\n  expire never;\n}\n", itf_name, preferred_ip);

        fd = open(dhclient_data->leasesfile.buffer, O_WRONLY | O_CREAT | O_APPEND, 0644);
        if(fd < 0) {
            SAH_TRACEZ_ERROR(ME, "Could not create file [%s] (%d: %s)", dhclient_data->leasesfile.buffer, errno, strerror(errno));
            goto exit;
        }
        if(write(fd, lease.buffer, lease.length - 1) < 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to write to lease file [%s](%d: %s)", dhclient_data->leasesfile.buffer, errno, strerror(errno));
        }
    }

    rc = 0;
exit:
    amxc_string_clean(&lease);
    return rc;
}

static int dhclient_start(const char* ns_name) {
    int rc = -1;
    amxc_var_t exec_args;
    amxc_var_init(&exec_args);

    dhclient_data_t* dhclient_data = dhclient_data_get(ns_name);
    if(!dhclient_data) {
        SAH_TRACEZ_ERROR(ME, "No dhclient data exists for namespace [%s]", ns_name);
        goto exit;
    }
    if(!dhclient_data->valid) {
        rc = 0;
        goto exit;
    }
    amxc_string_appendf(&dhclient_data->command, " >> %s 2>&1", dhclient_data->logfile.buffer);
    SAH_TRACEZ_INFO(ME, "Send dhcp command: %s", dhclient_data->command.buffer);

    amxc_var_set_type(&exec_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &exec_args, CTHULHU_SANDBOX_ID, ns_name);
    amxc_var_add_key(cstring_t, &exec_args, CTHULHU_CMD_SB_EXEC_CMD, dhclient_data->command.buffer);

    rc = amxb_call(cthulhu_get_amxb_bus_ctx(),
                   CTHULHU_DM_SANDBOX,
                   CTHULHU_CMD_SB_EXEC,
                   &exec_args,
                   NULL,
                   0);

exit:
    amxc_var_clean(&exec_args);
    return rc;
}

static char* dhclient_get_resolvfile(const char* ns_name) {
    char* resolv_file = NULL;

    dhclient_data_t* dhclient_data = dhclient_data_get(ns_name);
    if(!dhclient_data) {
        SAH_TRACEZ_ERROR(ME, "No dhclient data exists for namespace [%s]", ns_name);
        return NULL;
    }
    if(asprintf(&resolv_file, "%s/resolv.conf", dhclient_data->etc_dir.buffer) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not create string");
    }
    return resolv_file;
}

static int dhclient_stop(const char* ns_name) {
    int rc = -1;
    amxc_var_t exec_args;
    amxc_string_t stop_cmd;

    amxc_var_init(&exec_args);
    amxc_string_init(&stop_cmd, 0);

    dhclient_data_t* dhclient_data = dhclient_data_get(ns_name);
    if(!dhclient_data) {
        SAH_TRACEZ_ERROR(ME, "No dhclient data exists for namespace [%s]", ns_name);
        goto exit;
    }
    if(cthulhu_isfile(dhclient_data->pidfile.buffer)) {
        SAH_TRACEZ_ERROR(ME, "exists");
        if(cthulhu_isdir("/sys/kernel/security/apparmor")) {
            amxc_string_appendf(&stop_cmd, "aa-exec -p unconfined dhclient");
        } else {
            amxc_string_appendf(&stop_cmd, "dhclient");
        }
        amxc_string_appendf(&stop_cmd, " -r -v -d -pf %s -lf %s >> %s 2>&1",
                            dhclient_data->pidfile.buffer,
                            dhclient_data->leasesfile.buffer,
                            dhclient_data->logfile.buffer);
        SAH_TRACEZ_INFO(ME, "Send dhcp command: %s", stop_cmd.buffer);

        amxc_var_set_type(&exec_args, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &exec_args, CTHULHU_SANDBOX_ID, ns_name);
        amxc_var_add_key(cstring_t, &exec_args, CTHULHU_CMD_SB_EXEC_CMD, stop_cmd.buffer);

        rc = amxb_call(cthulhu_get_amxb_bus_ctx(),
                       CTHULHU_DM_SANDBOX,
                       CTHULHU_CMD_SB_EXEC,
                       &exec_args,
                       NULL,
                       0);
    }

exit:
    amxc_string_clean(&stop_cmd);
    amxc_var_clean(&exec_args);
    return rc;
}

static int dhclient_remove_ns(const char* ns_name) {
    dhclient_data_t* dhclient_data = NULL;
    dhclient_data = dhclient_data_get(ns_name);
    if(dhclient_data) {
        dhclient_data_delete(&dhclient_data);
    }
    return 0;
}

static dhcpc_client_funcs_t dhclient_functions = {
    .init = dhclient_init,
    .add_ns = dhclient_add_ns,
    .prestart = dhclient_prestart,
    .add_itf = dhclient_add_itf,
    .start = dhclient_start,
    .get_resolvfile = dhclient_get_resolvfile,
    .stop = dhclient_stop,
    .remove_ns = dhclient_remove_ns,
    .cleanup = dhclient_cleanup
};

dhcpc_client_funcs_t* dhclient_get_functions(void) {
    return &dhclient_functions;
}